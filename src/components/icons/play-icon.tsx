import React from 'react';
import Svg, { Path, Mask, G } from 'react-native-svg';

interface Props {
    width?: string | number;
    height?: string | number;
    color?: string;
  }

export const PlayIcon: React.FC<Props> = ({width = 24, height = 24, color = '#E8E8E8', ...others}) => {
    return (
        <Svg width={width} height={height} viewBox="0 0 24 24" fill="none" {...others}>
            <Path fill-rule="evenodd" clip-rule="evenodd" d="M10.956 18.0028C10.649 18.0028 10.339 17.9368 10.05 17.8008C9.402 17.4948 9 16.8878 9 16.2148V7.78878C9 7.11578 9.402 6.50878 10.05 6.20278C10.782 5.85778 11.65 5.95878 12.259 6.46178L17.358 10.6758C17.767 11.0118 18 11.4958 18 12.0018C18 12.5078 17.767 12.9918 17.358 13.3278L12.259 17.5408C11.891 17.8458 11.427 18.0028 10.956 18.0028Z" fill={color} />
            <Mask id="mask0" mask-type="alpha" x="9" y="6" width="9" height="13">
                <Path fill-rule="evenodd" clip-rule="evenodd" d="M10.956 18.0028C10.649 18.0028 10.339 17.9368 10.05 17.8008C9.402 17.4948 9 16.8878 9 16.2148V7.78878C9 7.11578 9.402 6.50878 10.05 6.20278C10.782 5.85778 11.65 5.95878 12.259 6.46178L17.358 10.6758C17.767 11.0118 18 11.4958 18 12.0018C18 12.5078 17.767 12.9918 17.358 13.3278L12.259 17.5408C11.891 17.8458 11.427 18.0028 10.956 18.0028Z" fill="white" />
            </Mask>
        </Svg>
    );
};
